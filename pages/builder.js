import React, { useState } from 'react';
import  Layout from '../src/Components/Layouts/Layout';
import { Grid, Box, TextField, Button } from '@mui/material';
import { makeStyles} from "@mui/styles";
import { useTheme } from '@emotion/react';
const IceCream=require('../src/Components/Core/Builders/IceCreamBuilder');

export const builder = () => {
    const classes = useStyles();
    const theme = useTheme();

    const nuevoHelado = new IceCream();
    const [formState, setFormState]= useState({flavors:'', recipent:'', size:'', toppings:'' })
    const [helado, setHelado]= useState(null);

    const handleSubmit = (e)=>{
        e.preventDefault();
        nuevoHelado.setFlavors(formState.flavors)
        .setRecipent(formState.recipent)
        .setSize(formState.size)
        .setToppings(formState.toppings);
        if(formState?.flavors?.length>0 || formState?.recipent?.length>0 ||formState?.size?.length>0 || formState?.toppings?.length>0){
            setHelado(nuevoHelado.create());
        }
    }
    
    
  return (
        <Layout seccion={'Builder Pattern'}>
            <Box
                component="form"
                onSubmit={handleSubmit}
                autoComplete="off"
            >
            <Grid item container spacing={3} style={{paddingBottom:theme.spacing(3)}}>
                <Grid item xs={12} sm={6} md={3}>
                    <TextField 
                        required
                        className={classes.input} 
                        id="outlined-basic" 
                        label="Sabores" 
                        variant="outlined"
                        value={formState.flavors}
                        onChange={(e)=>{
                           setFormState({...formState, flavors:e.target.value});
                        }} 
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <TextField 
                        required
                        className={classes.input} 
                        id="outlined-basic" 
                        label="Recipiente" 
                        variant="outlined"
                        value={formState.recipent}
                        onChange={(e)=>{
                           setFormState({...formState, recipent:e.target.value});
                        }} 
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <TextField 
                        required
                        className={classes.input} 
                        id="outlined-basic" 
                        label="Tamaño" 
                        variant="outlined"
                        value={formState.size}
                        onChange={(e)=>{
                           setFormState({...formState, size:e.target.value});
                        }} 
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <TextField 
                        required
                        className={classes.input} 
                        id="outlined-basic" 
                        label="Toppings" 
                        variant="outlined"
                        value={formState.toppings}
                        onChange={(e)=>{
                           setFormState({...formState, toppings:e.target.value});
                           console.log(formState);
                        }} 
                    />
                </Grid>
            </Grid>
            <Grid item container style={{paddingBottom:theme.spacing(3)}}>
                <Grid item xs={12}>
                    <Button 
                        type='submit'
                        className={classes.botonControl}
                    >
                        Crear helado
                    </Button>
                </Grid>
            </Grid>
                
            </Box>
            {/* es más simple invocar el componente con las props directamente
            sin embargo al ser un ejercicio académico usamos una clase para demostrar mejor
            el funcionamiento de un builder */}
            {helado}
            
        </Layout>
  )
}

const useStyles = makeStyles((theme) => ({
 input:{
     width:'100%'
 },
 botonControl:{
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    '&:hover':{
      background:theme.palette.secondary.main
    }
  },
}));

export default builder;