import { Grid, Typography } from '@mui/material';
import React, { useState } from 'react'
import Layout from '../src/Components/Layouts/Layout';
import { v4 as uuidv4 } from 'uuid';

const Iterador = require("../src/Components/Core/Iterator/Iterador");



export const iterator = () => {
    const iterador = new Iterador();
    iterador.fetchData();
    let cities = new Array;
        
        while(iterador.currentItem()){
            console.log(iterador.next())
            const currentItem = iterador.next();
            cities.push(
                <Grid item xs={3} key={uuidv4()}>
                    <Typography component="p" variant="caption">
                        {currentItem.city}
                    </Typography>
                </Grid>
             )
        }
    console.log('ciudades',cities);
    setTimeout(() =>{console.log('ciudades',cities);},2000)
  return (
    <Layout seccion={'Template Pattern'}>
        <Grid container>
            {cities}
        </Grid>
    </Layout>
  )
}

export default iterator;
