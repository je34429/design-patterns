import React from 'react'
import  ComponentTemplate from "../src/Components/Core/Templates/ComponentTemplate";
import Layout from '../src/Components/Layouts/Layout';

class CustomComponent extends ComponentTemplate{
    creating(obj){
        this.valor*=10;
        console.log(`creating desde custom: procesando ${obj.valor} a ${this.valor} ...`);
    }
}

export const template = () => {


  const componente1 = new CustomComponent().create({valor:10});

  return (
    <Layout seccion={'Template Pattern'}>
        <p>{componente1.valor}</p>

    </Layout>
  )
}

export default template;
