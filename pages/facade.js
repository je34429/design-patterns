import React, { useState } from 'react';
import  Layout from '../src/Components/Layouts/Layout';
import { Grid, Box, TextField, Button, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import { makeStyles} from "@mui/styles";
import { useTheme } from '@emotion/react';
const HamburgerLibrary = require('../src/Components/Core/Facade/HamburgerLibrary');
const HamburgerFacade = require('../src/Components/Core/Facade/HamburgerFacade');

export const Facade = () => {
    const classes = useStyles();
    const theme = useTheme();

    const hamburgerFacade = new HamburgerFacade(new HamburgerLibrary());
    
    const [formState, setFormState]= useState({
        bread:'blanco',
        meat:'res',
        cheese:'Mozzarella',
        bacon:false,
        sauces:'',
        lettuce:false,
        slicedTomato:false 
    })

    const [hamburguesa, setHamburguesa]= useState(null);

    const handleSubmit = (e)=>{
        e.preventDefault();
        console.log(formState);
        setHamburguesa(
            hamburgerFacade.makeBurguer(
                formState.bread,
                formState.meat,
                formState.cheese,
                formState.bacon,
                formState.sauces,
                formState.lettuce,
                formState.slicedTomato
            ));
    }
    
  return (
        <Layout seccion={'Facade Pattern'}>
            <Box
                component="form"
                onSubmit={handleSubmit}
                autoComplete="off"
            >
            <Grid item container spacing={3} style={{paddingBottom:theme.spacing(3)}}>
                <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth size="small" required>
                        <InputLabel>Tipo de pan</InputLabel>
                        <Select
                        value={formState.bread}
                        label="Tipo de pan"
                        onChange={(e)=>{
                            setFormState({...formState, bread:e.target.value});
                        }}
                        >
                        <MenuItem value={'blanco'}>Blanco</MenuItem>
                        <MenuItem value={'integral'}>Integral</MenuItem>
                        <MenuItem value={'sin gluten'}>Sin gluten</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth size="small" required>
                        <InputLabel>Tipo de carne</InputLabel>
                        <Select
                        value={formState.meat}
                        label="Tipo de carne"
                        onChange={(e)=>{
                            setFormState({...formState, meat:e.target.value});
                        }}
                        >
                        <MenuItem value={'res'}>Res</MenuItem>
                        <MenuItem value={'pollo'}>Pollo</MenuItem>
                        <MenuItem value={'a base de vegetales'}>Vegetales</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth size="small" required>
                        <InputLabel>Tipo de queso</InputLabel>
                        <Select
                        value={formState.cheese}
                        label="Tipo de queso"
                        onChange={(e)=>{
                            setFormState({...formState, cheese:e.target.value});
                        }}
                        >
                        <MenuItem value={'Mozzarella'}>Mozzarella</MenuItem>
                        <MenuItem value={'Holandes'}>Holandes</MenuItem>
                        <MenuItem value={'Sabana'}>Sabana</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth size="small" required>
                        <InputLabel>Tocino</InputLabel>
                        <Select
                        value={formState.bacon}
                        label="Tocino"
                        onChange={(e)=>{
                            setFormState({...formState, bacon:e.target.value});
                        }}
                        >
                        <MenuItem value={true}>Si</MenuItem>
                        <MenuItem value={false}>no</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <TextField 
                        required
                        className={classes.input} 
                        id="outlined-basic" 
                        label="Salsas" 
                        variant="outlined"
                        size="small"
                        value={formState.sauces}
                        onChange={(e)=>{
                           setFormState({...formState, sauces:e.target.value});
                        }} 
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth size="small" required>
                        <InputLabel>Lechuga</InputLabel>
                        <Select
                        value={formState.lettuce}
                        label="Tocino"
                        onChange={(e)=>{
                            setFormState({...formState, lettuce:e.target.value});
                        }}
                        >
                        <MenuItem value={true}>Si</MenuItem>
                        <MenuItem value={false}>no</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth size="small" required>
                        <InputLabel>Tomate</InputLabel>
                        <Select
                        value={formState.slicedTomato}
                        label="Tocino"
                        onChange={(e)=>{
                            setFormState({...formState, slicedTomato:e.target.value});
                        }}
                        >
                        <MenuItem value={true}>Si</MenuItem>
                        <MenuItem value={false}>no</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
            <Grid item container style={{paddingBottom:theme.spacing(3)}}>
                <Grid item xs={12}>
                    <Button 
                        type='submit'
                        className={classes.botonControl}
                    >
                        Crear hamburguesa
                    </Button>
                </Grid>
            </Grid>
                
            </Box>
            {/* es más simple invocar el componente con las props directamente
            sin embargo al ser un ejercicio académico usamos una clase para demostrar mejor
            el funcionamiento de un Facade */}
            {hamburguesa}
            
        </Layout>
  )
}

const useStyles = makeStyles((theme) => ({
 input:{
     width:'100%'
 },
 botonControl:{
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    '&:hover':{
      background:theme.palette.secondary.main
    }
  },
}));

export default Facade;