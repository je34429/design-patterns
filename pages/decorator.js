import React, { useState } from 'react';
import  Layout from '../src/Components/Layouts/Layout';
import { Grid, Box, TextField, Button, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import { makeStyles} from "@mui/styles";
import { useTheme } from '@emotion/react';
const Product = require('../src/Components/Core/Decorators/Product')
const ProductDecorator = require('../src/Components/Core/Decorators/ProductDecorator')
export const decorator = () => {
    const product = new Product({
        description:'El diseño del Huracán, esculpido y sensual, está inspirado en las formas hexagonales puntiagudas del átomo de carbono, mientras que el ADN Lamborghini se manifiesta claramente en la línea ininterrumpida que dibuja el perfil. El sistema de iluminación full LED le otorga una luz especialmente tecnológica que lo hace inconfundible incluso en la oscuridad. ', 
        name:'Lamborgini Huracán', 
        price:'800.000 EU'});
    const productComplex = new ProductDecorator(product.product);

    const classes = useStyles();
    const theme = useTheme();

    const [productFinal, setProductFinal] = useState(product.renderProduct());
    const [decorado, setDecorado] = useState(false);

    console.log(productComplex);
    const decorateToggle = () => {
      if(!decorado){
        setProductFinal(productComplex.renderProduct());
      }else{
        setProductFinal(product.renderProduct());
      }
      setDecorado(!decorado)
    }
  return (
        <Layout seccion={'Decorator Pattern'}>
          <Grid container className={classes.control}>
              <Grid item xs={12}>
              <Button 
                className={classes.botonControl}
                onClick={decorateToggle}
              >
                Decorar Toggle
              </Button>
              </Grid>
          </Grid>
          <Grid container>
              <Grid item xs={12}>
                {productFinal}
              </Grid>
          </Grid>
        </Layout>
  )
}

const useStyles = makeStyles((theme) => ({
 input:{
     width:'100%'
 },
 botonControl:{
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    '&:hover':{
      background:theme.palette.secondary.main
    }
  },
}));

export default decorator;