import { Button, FormControl, Grid, InputLabel, MenuItem, Select, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useState } from 'react';
import { CandyFactory } from '../src/Components/Core/Factories/CandyFactory'
import  Layout from '../src/Components/Layouts/Layout';

export const abstractFactory = () => {
  const classes = useStyles();
  const [stageCtx, setStageCtx] = useState('realWorld');
  const [candies, setCandies] = useState([]);
  const [candyType, setCandyType] = useState('caramel');
  
  const addCandy = (candyType) => {
    setCandies([...candies, candyType]);
  }

  const removeCandies = () =>{
    setCandies([]);
  }

  const changeContext = (ctx) =>{
    if(ctx==='realWorld'){
      setStageCtx('design')
    }else{
      setStageCtx('realWorld')
    }
  }
  

  return (
    <Layout seccion={'Abstract Factory Pattern'}>
      <Grid container className={classes.control} spacing={2}>
        <Grid item xs={6} sm={6} md={3}>
          <FormControl fullWidth size="small">
            <InputLabel>Tipo de dulce</InputLabel>
            <Select
              value={candyType}
              label="Tipo de dulce"
              onChange={(e)=>{
                setCandyType(e.target.value);
                console.log('el tipo de dulce es', e.target.value)
              }}
            >
              <MenuItem value={'caramel'}>Caramelo</MenuItem>
              <MenuItem value={'chocolate'}>Chocolate</MenuItem>
              <MenuItem value={'cookie'}>Galleta</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={6} sm={6} md={3}>
        <Button
            className={classes.botonControl}
            onClick={()=>{addCandy(candyType)}}
          >
            Agregar un dulce
        </Button>
        </Grid>
        <Grid item xs={6} sm={6} md={3}>
          <Button
            className={classes.botonControl}
            onClick={removeCandies}
          >
            Remover dulces
          </Button>
        </Grid>
        <Grid item xs={6} sm={6} md={3}>
          <Button
            className={classes.botonControl}
            onClick={()=>{changeContext(stageCtx)}}
          >
            Cambiar contexto
          </Button>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12}>
          <Typography component='h1' variant='h2' className={classes.title}>
            El contexto es <strong>el mundo {(stageCtx==='design')?'del diseño': 'real'}</strong>
          </Typography>
        </Grid>
      </Grid>

      <Grid container className={classes.candyGrid}>
        {candies.map((candy, index) => {
          return( 
              <Grid item xs={4} key={`candy-${index}`}>
                <CandyFactory ctx={stageCtx} candyType={candy} />
              </Grid>
            )
          })}
      </Grid>
    </Layout>
  )
}


export default abstractFactory;


const useStyles = makeStyles((theme) => ({
  botonControl:{
    textTransform:'inherit',
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    '&:hover':{
      background:theme.palette.secondary.main
    }
  },
  control:{
    textAlign:'center',
    paddingBottom:theme.spacing(4),
    marginBottom:theme.spacing(4),
    borderBottom:`1px solid ${theme.palette.common.lightGray}`
  },
  title:{
    textAlign:'center',
    paddingBottom:theme.spacing(4),
    marginBottom:theme.spacing(4),
    borderBottom:`1px solid ${theme.palette.common.lightGray}`
  },
  candyGrid:{
    alignItems:'center'
  }
}));