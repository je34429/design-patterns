
import { useTheme } from '@emotion/react'
import { Typography } from '@mui/material'
import {makeStyles } from '@mui/styles'
import { Box } from '@mui/system';
import Layout from '../src/Components/Layouts/Layout';

export function referencias() {

  const theme = useTheme();
  const classes = useStyles();
  return (
    <Layout seccion={'Referencias'}>
      <Box className={classes.caja}>
        <Typography component='h4' variant='h3' style={{color:theme.palette.primary.main}}>
        Referencias
        </Typography>

        <Typography component='h5' variant='h5'>
          React.js with Factory Pattern ? Building Complex UI With Ease
        </Typography>

        <Typography component='p' variant='body1'>
          https://dev.to/shadid12/react-js-with-factory-pattern-building-complex-ui-with-ease-1ojf
        </Typography>

        <Typography component='h5' variant='h5'>
          Patrón de diseño: patrón de fábrica
        </Typography>

        <Typography component='p' variant='body1'>
          https://dhtrust.org/instrucciones/patron-de-diseno-patron-de-fabrica/
        </Typography>

        <Typography component='h5' variant='h5'>
        Las PIEZAS FUNDAMENTALES de la PROGRAMACIÓN ORIENTADA A OBJETOS
        <br />
        (lista de reproducción sobre patrones de diseño)
        </Typography>
        
        <Typography component='p' variant='body1'>
          https://www.youtube.com/watch?v=3qTmBcxGlWk&list=PLJkcleqxxobUJlz1Cm8WYd-F_kckkDvc8&index=2
        </Typography>

        <Typography component='h5' variant='h5'>
        patrones de diseño en react (proyecto):
        </Typography>

        <Typography component='p' variant='body1'>
        https://github.com/themithy/react-design-patterns
        </Typography>

        <Typography component='h5' variant='h5'>
        Template Method
        </Typography>
        
        <Typography component='p' variant='body1'>
        https://refactoring.guru/es/design-patterns/template-method
        </Typography>

        <Typography component='h5' variant='h5'>
        Design Patterns - Template Method
        </Typography>
        
        <Typography component='p' variant='body1'>
        https://dev.to/carlillo/design-patterns---template-method-180k
        </Typography>

        <Typography component='h5' variant='h5'>
        React, una biblioteca de JavaScript para construir interfaces de usuario
        </Typography>
        
        <Typography component='p' variant='body1'>
        https://es.reactjs.org/
        </Typography>

        <Typography component='h5' variant='h5'>
        Next The React Framework for Production
        </Typography>
        
        <Typography component='p' variant='body1'>
        https://nextjs.org/
        </Typography>
        
        <Typography component='h5' variant='h5'>
        material ui una librería para IU en react
        </Typography>

        <Typography component='p' variant='body1'>
        https://mui.com/
        </Typography>

        <Typography component='h5' variant='h5'>
        Patrones de Diseño Javascript ES6 - Facade Pattern
        </Typography>

        <Typography component='p' variant='body1'>
        https://www.youtube.com/watch?v=ozzDSNrOm0A
        </Typography>

        <Typography component='h5' variant='h5'>
        Patrones de Diseño Javascript ES6 - Decorator (Decorator pattern)
        </Typography>

        <Typography component='p' variant='body1'>
        https://www.youtube.com/watch?v=lqLTYCYa01w
        </Typography>

        <Typography component='h5' variant='h5'>
        Patrones de Diseño Javascript ES6 - Iterator pattern
        </Typography>

        <Typography component='p' variant='body1'>
        https://www.youtube.com/watch?v=pI2VnT5Oq7I
        </Typography>

        <Typography component='h5' variant='h5'>
        The Complete Guide to JavaScript Classes
        </Typography>

        <Typography component='p' variant='body1'>
        https://dmitripavlutin.com/javascript-classes-complete-guide/
        </Typography>
      </Box>
    </Layout>
  )
}


const useStyles = makeStyles((theme) => ({
  caja:{
    '& p':{
      marginBottom:'20px'
    }
  },
}));

export default referencias;