import React, { useState } from 'react';
import  Layout from '../src/Components/Layouts/Layout';
import { Grid, Button, Icon } from '@mui/material';
import { makeStyles } from "@mui/styles";
import { Tree } from '../src/Components/Core/Composite/Tree';
import { v4 as uuidv4 } from 'uuid';


export const composite = () => {
    const classes = useStyles();

    const [trees, setTrees] = useState([]);

    const addTree = () => {
      setTrees([...trees, {id:uuidv4()}])
    }

    const destroyTree = (id) =>{
      console.log('destroy');
      setTrees(trees.filter(tree=>id!==tree.id))
    }

    

  return (
        <Layout seccion={'Composite Pattern'}>
          <Grid container className={classes.control}>
              <Grid item xs={12}>
              <Button 
                className={classes.botonControl}
                onClick={addTree}
              >
                Agregar <Icon>park</Icon>
              </Button>
              </Grid>
          </Grid>
         
        <Grid container>
          {trees?.map((tree,i) =>
            <Grid 
                item xs={3} 
                key={tree.id}
            >
              <Tree id={tree.id} destroyTree={destroyTree} />
            </Grid>
          )}
        </Grid>  
          
        </Layout>
  )
}

const useStyles = makeStyles((theme) => ({
  botonControl:{
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    '&:hover':{
      background:theme.palette.secondary.main
    }
  },
   control:{
    textAlign:'center',
    paddingBottom:theme.spacing(4),
    marginBottom:theme.spacing(4),
    borderBottom:`1px solid ${theme.palette.common.lightGray}`
  },
 input:{
     width:'100%'
 },
 
}));

export default composite;