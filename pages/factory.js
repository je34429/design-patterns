import { Button, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useState } from 'react';
import {CandyFactory} from '../src/Components/Core/Factories/CandyFactory'
import  Layout from '../src/Components/Layouts/Layout';

export const factoryPattern = () => {
  const classes = useStyles();
  const [stageCtx, setStageCtx] = useState('realWorld');
  const [candies, setCandies] = useState([])
  
  const addCandy = (type) => {
    setCandies([...candies, type]);
  }

  const removeCandies = () =>{
    setCandies([]);
  }

  const changeContext = (ctx) =>{
    if(ctx==='realWorld'){
      setStageCtx('design')
    }else{
      setStageCtx('realWorld')
    }
  }

  return (
    <Layout seccion={'Factory Pattern'}>
      <Grid container className={classes.control}>
        <Grid item xs={12} sm={4}>
          <Button
            className={classes.botonControl}
            onClick={()=>{addCandy('caramel')}}
          >
            Agregar un dulce
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button
            className={classes.botonControl}
            onClick={removeCandies}
          >
            Remover dulces
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button
            className={classes.botonControl}
            onClick={()=>{changeContext(stageCtx)}}
          >
            Cambiar contexto
          </Button>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12}>
          <Typography component='h1' variant='h2' className={classes.title}>
            El contexto es <strong>el mundo {(stageCtx==='design')?'del diseño': 'real'}</strong>
          </Typography>
        </Grid>
      </Grid>

      <Grid container>
        {candies.map((candy, index) => {
          return( 
              <Grid item xs={4} key={`candy-${index}`}>
                <CandyFactory ctx={stageCtx} candyType={candy} />
              </Grid>
            )
          })}
      </Grid>
    </Layout>
  )
}

const useStyles = makeStyles((theme) => ({
  botonControl:{
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    '&:hover':{
      background:theme.palette.secondary.main
    }
  },
  control:{
    textAlign:'center',
    paddingBottom:theme.spacing(4),
    marginBottom:theme.spacing(4),
    borderBottom:`1px solid ${theme.palette.common.lightGray}`
  },
  title:{
    textAlign:'center',
    paddingBottom:theme.spacing(4),
    marginBottom:theme.spacing(4),
    borderBottom:`1px solid ${theme.palette.common.lightGray}`
  }
}));
export default factoryPattern;