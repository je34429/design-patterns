import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React, { useEffect, useState } from 'react'
import Observable from '../src/Components/Core/Observers/Observable';
import Layout from '../src/Components/Layouts/Layout';
import { v4 as uuidv4 } from 'uuid';

class ObservableNumber extends Observable {
    constructor() {
        super();
        this.value = 0;
    }

    getValue(){
        return this.value;
    }

    increment() {
        this.value++;
        this.notify(this);
    }
}

class Subscriber {
    constructor(){
        this.id = uuidv4();
    }
    notify(model) {
        console.log(`Puedo observar que el valor cambio a ${model.value}`);
    }
}

const observableNumber = new ObservableNumber();
const suscriptor = new Subscriber();
observableNumber.subscribe(suscriptor);
// observableNumber.unsubscribe(suscriptor.id);

const observer = () => {
    const classes=useStyles();

    const [valor, setValor]= useState(observableNumber.getValue());
    // se suscribe el suscriptor
    
   
const handleClick=()=>{
    observableNumber.increment();
    setValor(observableNumber.getValue());
}
  return (
    <Layout seccion={'Observable Pattern'}>
        <p>{console.log(observableNumber)}</p>
        <p>{valor}</p>
        <Button className={classes.botonObserver} onClick={handleClick}>
            Aumentar valor
        </Button>
    </Layout>
  )
}

const useStyles = makeStyles((theme) => ({
    botonObserver: {
      background:theme.palette.primary.main,
      color: theme.palette.common.white,
      '&:hover':{
          background:theme.palette.secondary.main
      }
    },
}));

export default observer;
