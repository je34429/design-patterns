
import { useTheme } from '@emotion/react'
import { Typography } from '@mui/material'
import Head from 'next/head'
import Layout from '../src/Components/Layouts/Layout';

export default function Home() {
  const theme = useTheme();
  return (
    <Layout seccion={'Inicio'}>
      <Typography component='h4' variant='h3' style={{color:theme.palette.primary.main}}>
        PATRONES ESTÁNDARES Y METODOLOGÍAS PARA LA CONSTRUCCIÓN DE SOFTWARE
      </Typography>
      <Typography component='h1' variant='h2' style={{color:theme.palette.primary.main}}>
        Actividad 5 | Patrones de diseño
      </Typography>
      

      <Typography component='p' variant='body1'>
        Docente: Ingeniero Hernan Dario Cruz Bueno
      </Typography>

      <Typography component='h5' variant='h5'>
        Ingeniería de software - Universidad Manuela Beltrán
      </Typography>

      <Typography component='p' variant='body1'>
        Elaborado por: Jaime Enrique Rodríguez Castellanos
      </Typography>

    </Layout>
  )
}
