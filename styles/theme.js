import {createTheme} from '@mui/material/styles';

const theme = createTheme({
    palette:{
        primary:{
            main:'#095C4A'
        },
        secondary:{
            main:'#19857b',
        },
        error:{
            main:'#930C0C'
        },
        common:{
            lightGray:'#dddddd',
            surfaces:'#E0E0E0'
        }
    },
    shadows: [
        `0px 1px 2px 0px rgba(129,146,162,0.2)`,
        `0px 3px 4px 0px rgba(129,146,162,0.2)`,
        `0px 6px 6px 0px rgba(129,146,162,0.2)`,
        `0px 9px 8px 0px rgba(129,146,162,0.2)`,
        `0px 12px 10px 0px rgba(129,146,162,0.2)`,
        `0px 15px 12px 0px rgba(129,146,162,0.2)`,
        `0px 18px 14px 0px rgba(129,146,162,0.2)`,
        `0px 18px 16px 0px rgba(129,146,162,0.2)`,
        `0px 18px 18px 0px rgba(129,146,162,0.2)`,
        `0px 18px 20px 0px rgba(129,146,162,0.2)`,
        `0px 18px 22px 0px rgba(129,146,162,0.2)`,
        `0px 18px 24px 0px rgba(129,146,162,0.2)`,
        `0px 18px 26px 0px rgba(129,146,162,0.2)`,
        `0px 18px 28px 0px rgba(129,146,162,0.2)`,
        `0px 18px 30px 0px rgba(129,146,162,0.2)`,
        `0px 18px 32px 0px rgba(129,146,162,0.2)`,
        `0px 18px 34px 0px rgba(129,146,162,0.2)`,
        `0px 18px 36px 0px rgba(129,146,162,0.2)`,
        `0px 18px 38px 0px rgba(129,146,162,0.2)`,
        `0px 18px 40px 0px rgba(129,146,162,0.2)`,
        `0px 18px 42px 0px rgba(129,146,162,0.2)`,
        `0px 18px 44px 0px rgba(129,146,162,0.2)`,
        `0px 18px 46px 0px rgba(129,146,162,0.2)`,
        `0px 18px 48px 0px rgba(129,146,162,0.2)`,
        `0px 18px 50px 0px rgba(129,146,162,0.2)`,
      ],
});

export default theme;
