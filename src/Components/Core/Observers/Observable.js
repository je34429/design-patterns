class Observable {
    constructor() {
        this.observers = [];
    }

    subscribe(c) {
        this.observers.push(c);
    }

    unsubscribe(c) {
         this.observers = this.observers.filter(observer => observer.id === c.id);
    }

    notify(model) {
        this.observers.forEach(observer => {
            observer.notify(model);
        });
    }
}



export default Observable;