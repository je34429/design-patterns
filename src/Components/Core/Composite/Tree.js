import {useEffect, useState} from 'react';
import { Box, Button, Grid, Icon } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { v4 as uuidv4 } from 'uuid';
import { Leaf } from './Leaf';

export const Tree = ({id, destroyTree}) => {
  const classes = useStyles({id});
  const [color, setColor] = useState('#000');
  const [leafs, setLeafs] = useState([])
  const [leafCount, setLeafCount] = useState(0);
  const addLeaf = ()=>{
    setLeafCount(leafCount+=1);
    setLeafs([...leafs, {id:uuidv4(), num:leafCount}])
  }

  const destroyLeaf = (id) =>{
    console.log('destroy');
    setLeafs(leafs.filter(leaf=>id!==leaf.id))
  }

  const colorTree=()=>{
    const getRgb = ()=>Math.floor(Math.random()*256);
    const rgbToHex = (r,g,b)=> {
      const hexCode = [r,g,b].map(x=>{
        const hex = x.toString(16);
        return hex.length ===1?`0${hex}`:hex;
      }).join('')
      return `#${hexCode}`
    }
    
    setColor(
      rgbToHex(
        getRgb(),
        getRgb(),
        getRgb(),
      )
    )

  }

  useEffect(() => {
    let mounted = true;

    if(mounted){
      colorTree()
    }
    return () => {
      mounted = false;
    }
  }, [])
  

  return (
    <Box className={classes.tree}>
      <Grid container className={classes.treeControl}>
          <Grid item xs={4}>
            <Button 
              className={classes.botonTree}
              onClick={addLeaf}
            >
              <Icon style={{fontSize:'12px'}}>energy_savings_leaf</Icon> +
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button 
              className={classes.botonTree}
              onClick={colorTree}
            >
              <Icon style={{fontSize:'12px'}}>palette</Icon>+
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button 
              className={classes.botonTree}
              onClick={()=>{destroyTree(id)}}
            >
              <Icon style={{fontSize:'12px'}}>park</Icon>-
            </Button>
          </Grid>
      </Grid>
      
      <Grid container>
        {leafs.map((leaf)=>
          <Grid item xs={3}key={leaf.id}>
            <Leaf 
              id={leaf.id} 
              destroy={destroyLeaf} 
              leafNum={leaf.num}
              color={color}
            />
          </Grid>
        )}
      </Grid>
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  botonTree: {
    background:theme.palette.primary.main,
    color: theme.palette.common.white,
    margin:'5px'
  },
  tree:{
    background:theme.palette.common.lightGray,
    margin:theme.spacing(1),
    borderRadius:'5px',
    boxShadow:theme.shadows[4],
    padding:theme.spacing(1)
  },
  treeControl:{
    textAlign:'center',
    paddingBottom:theme.spacing(1),
    marginBottom:theme.spacing(1),
    borderBottom:'1px solid rgba(255,255,255,0.35)'
  }
}));