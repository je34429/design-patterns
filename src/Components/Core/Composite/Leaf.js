import { Button, Icon, Typography } from '@mui/material';
import {makeStyles} from '@mui/styles';
import { Box } from '@mui/system';
import React from 'react'

export const Leaf = ({id, destroy, leafNum, color='#000'}) => {
  const classes = useStyles();

  return (
    <Button 
      className={classes.botonLeaf}
      style={{background:color}} 
      onClick={
          ()=>{destroy(id)
      }}
    >
      <Icon style={{fontSize:'17px', lineHeight:'18px'}}>energy_savings_leaf</Icon>
      <Typography variant='caption' component='p'>{leafNum}</Typography>
    </Button>
  )
}

const useStyles = makeStyles((theme) => ({
  botonLeaf: {
    display: 'block',
    background:theme.palette.primary.main,
    color: theme.palette.common.white,
    width:'55px',
    height: '55px',
    minWidth:'55px',
    borderRadius:'50%',
    padding:'0px',
    textAlign:'center',
    margin:'3px',
    fontSize:'17px', lineHeight:'18px',
    '&:hover':{
      background:theme.palette.secondary.main
    }
  }
}));

export default Leaf;