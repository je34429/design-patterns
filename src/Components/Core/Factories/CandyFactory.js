import {CandyDesignFactory} from './CandyDesignFactory'
import {CandyRealFactory} from './CandyRealFactory'

export const CandyFactory = ({ctx, candyType})=>{
    switch(ctx){
      case 'design':
        return <CandyDesignFactory type={candyType}/>
      case 'realWorld':
        return <CandyRealFactory type={candyType}/>
    }
  }




