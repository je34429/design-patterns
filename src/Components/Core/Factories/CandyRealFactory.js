
import { makeStyles } from "@mui/styles";
import { CaramelReal } from "../Candies/CaramelReal";
import { ChocolateReal } from "../Candies/ChocolateReal";
import { CookieReal } from "../Candies/CookieReal";

export const CandyRealFactory = ({type='caramel'}) => {
  const classes = useStyles();
  
  const candyRealFactory = (realType)=> {
    switch(realType){
      case 'caramel':
        return <CaramelReal />
      case 'chocolate':
        return <ChocolateReal />
      case 'cookie':
        return <CookieReal />
    }
  }
  return candyRealFactory(type);
  
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
  }
}));



