
import { makeStyles } from "@mui/styles";
import { CaramelDesign } from "../Candies/CaramelDesign";
import { ChocolateDesign } from "../Candies/chocolateDesign";
import { CookieDesign } from "../Candies/CookieDesign";

export const CandyDesignFactory = ({type='caramel'}) => {
  const classes = useStyles();
  const candyDesignFactory = (designType)=> {
    switch(designType){
      case 'caramel':
        return <CaramelDesign />
      case 'chocolate':
        return <ChocolateDesign />
      case 'cookie':
        return <CookieDesign />
    }
  }
  return candyDesignFactory(type);
  
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
  }
}));



