
import { makeStyles } from "@mui/styles";
import { Box } from '@mui/material';
import Image from 'next/image'

export const CaramelDesign = () => {
  const classes = useStyles();
  return (
    <Box className={classes.candyBox}>
        <Image
          src="/images/candy-illustration.png"
          alt="candy"
          width="900px"
          height="689px"
          blurDataURL={'/images/candy-illustration.png'}
          placeholder="blur"
        />
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
  }
}));



