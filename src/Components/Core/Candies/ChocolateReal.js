
import { makeStyles } from "@mui/styles";
import { Box } from '@mui/material';
import Image from 'next/image'

export const ChocolateReal = () => {
  const classes = useStyles();
  return (
    <Box className={classes.candyBox}>
        <Image
          src="/images/chocolate-real.png"
          alt="candy"
          width="900px"
          height="868px"
          blurDataURL={'/images/chocolate-real.png'}
          placeholder="blur"
        />
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
  }
}));



