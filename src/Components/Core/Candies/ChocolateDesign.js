
import { makeStyles } from "@mui/styles";
import { Box } from '@mui/material';
import Image from 'next/image'

export const ChocolateDesign = () => {
  const classes = useStyles();
  return (
    <Box className={classes.candyBox}>
        <Image
          src="/images/chocolate-illustration.png"
          alt="candy"
          width="900px"
          height="1103px"
          blurDataURL={'/images/chocolate-illustration.png'}
          placeholder="blur"
        />
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    
  }
}));



