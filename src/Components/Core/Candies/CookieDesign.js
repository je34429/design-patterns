
import { makeStyles } from "@mui/styles";
import { Box } from '@mui/material';
import Image from 'next/image'

export const CookieDesign = () => {
  const classes = useStyles();
  return (
    <Box className={classes.candyBox}>
        <Image
          src="/images/galleta-illustration.png"
          alt="candy"
          width="900px"
          height="546px"
          blurDataURL={'/images/galleta-illustration.png'}
          placeholder="blur"
        />
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
  }
}));



