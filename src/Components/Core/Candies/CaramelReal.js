
import { makeStyles } from "@mui/styles";
import { Box } from '@mui/material';
import Image from 'next/image'

export const CaramelReal = () => {
  const classes = useStyles();
  return (
    <Box className={classes.candyBox}>
        <Image
          src="/images/candy-real.png"
          alt="candy"
          width="900px"
          height="600px"
          blurDataURL={'/images/candy-real.png'}
          placeholder="blur"
        />
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
  }
}));



