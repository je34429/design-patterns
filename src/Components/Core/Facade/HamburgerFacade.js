
class HamburgerFacade {
    constructor(HamburgerLibInstance){
        this.hamburgerLibrary = HamburgerLibInstance;
    }

    makeBurguer(bread,meat,cheese,bacon,sauces,lettuce,slicedTomato){
        
        return this.hamburgerLibrary
        .setMeat(meat)
        .setBread(bread)
        .setCheese(cheese)
        .setBacon(bacon)
        .setSauces(sauces)
        .setLettuce(lettuce)
        .setSlicedTomato(slicedTomato)
        .cook();
    }

}

module.exports = HamburgerFacade;