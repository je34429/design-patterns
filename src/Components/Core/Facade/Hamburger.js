import React from 'react'
import { Typography, Grid, Box } from '@mui/material';
import Image from 'next/image'
import { makeStyles, useTheme } from "@mui/styles";

export const Hamburger = ({meat, bread, cheese, bacon, sauces, lettuce, slicedTomato }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Grid container className={classes.wrapper}>
      
      <Grid item xs={12} md={4}>
        <Box className={classes.candyBox}>
          <Image
            src="/images/hamburger.png"
            alt="candy"
            width="900px"
            height="826px"
            blurDataURL={'/images/hamburger.png'}
            placeholder="blur"
          />
        </Box>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography 
          variant='body1' 
          style={{
              textAlign:'justify', padding:theme.spacing(4)
          }}
        >
          Su Hamburguesa viene en
            {(bread)&&<> <strong>pan {bread}</strong>,</>}
            {(meat)&&<> su tipo de carne es <strong>{meat}</strong>;</>}
            {(cheese)&&<> lleva queso <strong>{cheese}</strong> y </>}
            {(bacon)&&<> <strong>tocino</strong>,</>}
            {(sauces)&&<> salsas: <strong>{sauces}</strong>;</>}
            {(lettuce)&&<> viene con <strong>lechuga fresca</strong></>}
            {(slicedTomato)&&<> y tomate rebanado</>}
        </Typography>
      </Grid>
    </Grid>
    
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
    maxWidth:'380px',
    margin:'20px auto'
  },
  wrapper:{
    alignItems:'center'
  }
}));