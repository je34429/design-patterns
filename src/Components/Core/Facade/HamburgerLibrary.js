const { Hamburger } = require("./Hamburger");


class HamburgerLibrary {
        
        constructor(burguerInstance){
            this.hamburger = {};
        }

        setMeat(meat){
            this.hamburger.meat = meat;
            return this;
        } 

        setBread(bread){
            this.hamburger.bread = bread;
            return this;
        } 
        
        setCheese(cheese){
            this.hamburger.cheese = cheese;
            return this;
        } 
        
        setBacon(bacon){
            this.hamburger.bacon = bacon;
            return this;
        } 
        
        setSauces(sauces){
            this.hamburger.sauces = sauces;
            return this;
        } 
        
        setLettuce(lettuce){
            this.hamburger.lettuce = lettuce;
            return this;
        }

        setSlicedTomato(slicedTomato){
            this.hamburger.slicedTomato = slicedTomato;
            return this;
        }

        cook(){
            return( 
                <Hamburger 
                    meat={this.hamburger.meat}
                    bread={this.hamburger.bread}
                    cheese={this.hamburger.cheese}
                    bacon={this.hamburger.bacon}
                    sauces={this.hamburger.sauces}
                    lettuce={this.hamburger.lettuce}
                    slicedTomato={this.hamburger.slicedTomato}
                    
                />
            )
        }
    }

module.exports = HamburgerLibrary;