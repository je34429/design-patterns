import { Grid, Typography } from '@mui/material';
import { makeStyles, useTheme } from '@mui/styles'
import { Box } from '@mui/system';
import Image from 'next/image';
import React from 'react'

export const ProductComplex = ({name, price, description, image}) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box>
      <Grid container>
        <Grid item xs={12}>
            <Box className={classes.imageBox}>
                <Image
                  src={image}
                  alt="car"
                  width="900px"
                  height="675px"
                  blurDataURL={image}
                  placeholder="blur"
                />
            </Box>
        </Grid>
        <Grid item xs={12}>
          <Box className={classes.body}>
            <Typography component="h3" variant="h3" style={{marginBottom:theme.spacing(2)}}>
              <strong>{name}</strong>
            </Typography>
            <Typography component="p" variant="p" style={{color:'#666', marginBottom:theme.spacing(2)}}>
              {description}
            </Typography>
            <Typography component="p" variant="caption" style={{color:theme.palette.primary.main}}>
              <strong>{price}</strong>
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  imageBox:{
    textAlign:'center',
    position:'relative',
    width:'100%',
  },
  body:{

  }

}))
