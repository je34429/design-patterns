const { ProductSimple } = require("./ProductSimple");

class Product {
    constructor(productInstance){
        this.product = productInstance;
    }

    setPrice(price){
        this.product.price = price
        return this;
    }

    setName(name){
        this.product.name = name
        return this;
    }

    setDescription(description){
        this.product.description = description
        return this;
    }

    renderProduct(){
        return(
            <ProductSimple 
                name={this.product.name} 
                price={this.product.price} 
                description={this.product.description}
                image={'/images/car-simple.png'}
                />
        );
    }
};

module.exports = Product;