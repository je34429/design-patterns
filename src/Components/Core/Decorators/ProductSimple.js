import { Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles'
import { Box } from '@mui/system';
import Image from 'next/image';
import React from 'react'

export const ProductSimple = ({name, price, description, image}) => {
  const classes = useStyles();
  return (
    <Box>
      <Grid container>
        <Grid item xs={12}>
            <Box className={classes.imageBox}>
                <Image
                  src={image}
                  alt="car"
                  width="900px"
                  height="675px"
                  blurDataURL={image}
                  placeholder="blur"
                />
            </Box>
        </Grid>
        <Grid item xs={12}>
          <Box className={classes.body}>
            <Typography component="h4" variant="h4">
              {name}
            </Typography>
            <Typography component="p" variant="p">
              {description}
            </Typography>
            <Typography component="p" variant="caption">
              {price} 
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}

const useStyles = makeStyles((theme) => ({
  imageBox:{
    textAlign:'center',
    position:'relative',
    width:'100%',
  },
  body:{

  }

}))
