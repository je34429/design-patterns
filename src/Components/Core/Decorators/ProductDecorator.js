const { ProductComplex } = require("./ProductComplex");

class ProductDecorator {
    constructor(product){
        this.product = product;
    }

    renderProduct(){
        return(
            <ProductComplex 
                name={this.product.name} 
                price={this.product.price} 
                description={this.product.description}
                image={'/images/car-complex.png'}
            />
        );
    }

};

module.exports = ProductDecorator;