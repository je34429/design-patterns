import React from 'react'
import { Typography, Grid, Box } from '@mui/material';
import Image from 'next/image'
import { makeStyles, useTheme } from "@mui/styles";

export const IceCream = ({flavors, recipent,toppings, size}) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Grid container className={classes.wrapper}>
      
      <Grid item xs={12} md={4}>
        <Box className={classes.candyBox}>
          <Image
            src="/images/helado.png"
            alt="candy"
            width="900px"
            height="1217px"
            blurDataURL={'/images/helado.png'}
            placeholder="blur"
          />
        </Box>
      </Grid>
      <Grid item xs={12} md={8}>
        <Typography 
          variant='body1' 
          style={{
              textAlign:'justify', padding:theme.spacing(4)
          }}
        >
          Su helado 
            {(flavors)&&<> tiene los siguientes sabores: <strong>{flavors}</strong>,</>}
            {(recipent)&&<> esta servido en <strong>{recipent}</strong></>}
            {(size)&&<> es de tamaño <strong>{size}</strong>,</>}
            {(toppings)&&<> esta cubierto con <strong>{toppings}</strong></>}
        </Typography>
      </Grid>
    </Grid>
    
  )
}

const useStyles = makeStyles((theme) => ({
  candyBox:{
    position:'relative',
    width:'100%',
    maxWidth:'380px',
    margin:'20px auto'
  },
  wrapper:{
    alignItems:'center'
  }
}));