import {IceCream} from './IceCream'

class IceCreamBuilder {
        setFlavors(flavors){
            this.flavors = flavors;
            return this;
        }
        setRecipent(recipent){
            this.recipent = recipent;
            return this;
        }
        setToppings(toppings){
            this.toppings = toppings;
            return this;
        }
        setSize(size){
            this.size = size;
            return this;
        }
        create(){
            return( 
                <IceCream 
                    flavors={this.flavors}
                    recipent={this.recipent}
                    toppings={this.toppings}
                    size={this.size} 
                />
            )
        }
    }

module.exports = IceCreamBuilder;