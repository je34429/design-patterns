class ComponentTemplate {

    create(obj){
        this.beforeCreate(obj);
        this.creating(obj);
        this.created(obj);
        return this;     
    }

    beforeCreate(obj){
        
        this.valor= obj.valor+10;
        console.log(`beforeCreate: procesando ${obj.valor} a ${this.valor} ...`);
        
    }

    creating(obj){
        
        this.valor+=10;
        console.log(`creating: procesando ${obj.valor} a ${this.valor} ...`);
    }

    created(obj){
        this.valor+=10;
        console.log(`created: procesando ${obj.valor} a ${this.valor} ...`);
    }
};







export default ComponentTemplate;