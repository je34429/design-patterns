import { Box, Container, Button, Grid } from "@mui/material";
import { makeStyles } from "@mui/styles";
import {useRouter} from 'next/router';

export const TopBar = ({seccion='sin seccion'}) => {

  const classes = useStyles();
  const router = useRouter();
  return (
    <Box className={classes.topBarWrapper}>
      <Container >
        <Grid container className={classes.gridTop}>
          <Grid item xs={4}>{seccion}
            
          </Grid>
          <Grid item xs={8}>
            <Button onClick={()=>{router.push('/factory')}} className={classes.boton}>1</Button>
            <Button onClick={()=>{router.push('/abstract-factory')}} className={classes.boton}>2</Button>
            <Button onClick={()=>{router.push('/builder')}} className={classes.boton}>3</Button>
            <Button onClick={()=>{router.push('/composite')}} className={classes.boton}>4</Button>
            <Button onClick={()=>{router.push('/facade')}} className={classes.boton}>5</Button>
            <Button onClick={()=>{router.push('/decorator')}} className={classes.boton}>6</Button>
            <Button onClick={()=>{router.push('/template')}} className={classes.boton}>7</Button>
            <Button onClick={()=>{router.push('/observer')}} className={classes.boton}>8</Button>
            <Button onClick={()=>{router.push('/iterator')}} className={classes.boton}>9</Button>
            <Button onClick={()=>{router.push('/referencias')}} className={classes.boton}>10</Button>
          </Grid>
        </Grid>
      </Container> 
    </Box>
  )
}


const useStyles = makeStyles((theme) => ({
  topBarWrapper:{
    background:theme.palette.primary.main,
    color:theme.palette.common.white,
    padding:theme.spacing(3),
    
  },
  gridTop:{
    textAlign:'center',
    alignItems:'center',
    justifyContent:'center'
  },
  boton:{
    background:'#fff',
    widht:'45px',
    height:'45px',
    minWidth:'45px',
    borderRadius:'50%',
    margin:'0px 5px',
    '&:hover':{
      background:theme.palette.secondary.main,
      color:'#fff'
    }
  }
}));

export default TopBar;