
import { Container, Grid } from '@mui/material';
import {makeStyles} from '@mui/styles';
import Head from 'next/head';
import TopBar from '../Core/TopBar';


export const Layout = ({children, seccion}) => {
 const classes = useStyles();
    return (
        <>
        <Head>
          <title>Actividad 5</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
          <Grid container className={`${classes.root}`} direction="column">
            <Grid item container direction="column" xs={12}>
              <Grid item xs={12}> 
                  <TopBar seccion={seccion} />
              </Grid>
              <Grid item xs={12}>
                <Container maxWidth="lg" className={classes.contenedorPrincipal}>
                    {children}
                </Container>
              </Grid>
            </Grid>
          </Grid>
        </>
  )
}

const useStyles = makeStyles((theme)=>({
    contenedorPrincipal:{
      paddingTop:theme.spacing(5),
    },

}));

export default Layout;